<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		 <link rel="stylesheet" href="/Vue/Connexion/Connexion.css" />
		
        <title>Connexion impossible</title>
	</head>

    <body class='IsHidden'>
        <div id="bloc_page">
		
            <?php include ('../../Vue/Index/entete.php'); ?>
			<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
			
			<div id="bas">
            
				<form method="post" action="../../Controleur/Connexion/Connexion.php">
					<p>
						Pseudo ou mot de passe inconnu<br /><br />
						<label for="pseudo">Pseudo :</label> <input type="text" name="pseudo" id="pseudo" required /><br />
						<label for="pass">Mot de passe :</label> <input type="password" name="pass" id="pass" required /><br />
						<label for="auto">Connexion automatique</label><input type="checkbox" name="auto" id="auto" /><br />
						<input type="submit" value="Envoyer" />
					</p>
				</form>
				
				<aside id="pop">
						<div>
							<h1>Séries populaires</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="premium">
						<div>
							<h1>Séries Premium</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="new">
						<div>
							<h1>Nouvelles séries</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="hausse">
						<div>
							<h1>Séries en hausse</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
			</div>
			
            <footer>
                <div id="Footer site">
                    <h1>FYNS</h1>
                    <h2>Find Your Next Show</h2>
                    <p>Crée par Eric de La Varende</p>
                </div>
                <div id="Liens">
                    <h1>Liens utiles</h1>
						<ul>
							<li><a href="#">Accueil</a></li>
							<li><a href="#">Plan du site</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Séries</a></li>
						</ul>
                </div>
                <div id="Nouveaux membres">
                    <h1>Nouveaux membres</h1>
                    <ul>
                        <li>Identifiant 1</li>
                        <li>Identifiant 2</li>
                        <li>Identifiant 3</li>
                        <li>Identifiant 4</li>
                    </ul>
                </div>
            </footer>
        </div>
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/min.js"></script>
		
    </body>
</html>