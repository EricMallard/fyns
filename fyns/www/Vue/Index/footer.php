<footer>
	<div id="Footer_site">
		<h1>FYNS</h1>
		<h2>Find Your Next Show</h2>
		<p>Crée par Eric de La Varende</p>
	</div>
	<div id="Liens">
		<h1>Liens utiles</h1>
			<ul>
				<li><a href="#">Accueil</a></li>
				<li><a href="#">Plan du site</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">Séries</a></li>
			</ul>
	</div>
	<div id="Nouveaux_membres">
		<h1>Nouveaux membres</h1>
		<ul>
			<li>Identifiant 1</li>
			<li>Identifiant 2</li>
			<li>Identifiant 3</li>
			<li>Identifiant 4</li>
		</ul>
	</div>
</footer>