
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		<link rel="stylesheet" href="/Controleur/Index/Index.css" />
		<link rel="icon" type="image/png" href="/FYNS.png" />
        <title>FYNS</title>
    </head>

    <body >
        
		
            <?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>
            
            <?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
            
		<div id="bas">
			
				<section id="prez">
						
					
					<div>
						<h1>FYNS, c'est quoi?</h1>
						<p>L'idée est simple. Le petit monde des séries se développe d'année en année. A chaque début de cycle, que ce soit à la rentrée en septembre, au début de l'année en janvier ou pendant l'été, un nombre grandissant de "TV shows" nous est proposé. Entre les plaisirs coupables, les grandes fresques historiques, les sitcoms, les séries sci-fi, les reboot et spin-off à n'en plus finir il y en a pour tous les goûts. Et c'est bien le soucis...</p>
						<p>Pourtant on a chacun un ami qui les connait toutes ces séries. Du moins il a vu quelques épisodes de chacune. Quand on le voit, on lui laisse 2 3 minutes pour présenter ses séries du moment, au cas où il aurait eu une révélation. Un "binge-watching" qui s'est bien fini avec une nouvelle référence à la clé, comme GoT il y a quelques années. Mais la vérité c'est qu'on le laisse parler surtout pour lui faire plaisir. On le relance un peu, on fait semblant d'en noter une ou deux, mais on l'écoute de moins en moins. Le problème c'est que 3 fois sur 4 ce qu'il nous a proposé, ça ne collait pas.<br />
						</p><p>Community, "la meilleure série du monde" : Mmmmh, j'ai des doutes!<br />
						The Walking Dead, "une tension de tous les instants" : oui...1 épisode sur 4...sans prendre en compte la saison 2!<br />
						Black Mirror, "une vision glaçante des futures dérives de la technologie" : Ok, c'est une série ou pas? Pourquoi on change de personnage et d'intrigue à chaque épisode?<br />
						Chosen : trop violent. Daredevil : décevant. The Leftovers : étrange.<br />
						Bref, quelques bonnes surprises mais surtout beaucoup d'arrêt prématurés.</p>
						<p><strong>Alors comment choisir?</strong> Evidemment on peut trouver des avis éclairés pour chaque série sur internet, mais on en revient toujours au même problème : ceux qui nous proposent des séries ou commentent ont rarement la même opinion que nous.</p>
						<p>J'avais dit que l'idée était simple, la voici : FYNS va maintenant vous <strong>suggérer</strong> de manière <strong>personnalisée</strong> des séries sans que vous n'ayez rien à faire ou presque!</p>
					</div>
			
					<div>
						<h1>Le concept</h1>
						<p><strong>Dans les faits comment ça marche?</strong></p>
						<p>Le site n'a besoin que d'une chose : connaitre <a href='/Controleur/Faq/Faq.php#la_notation'>vos avis</a> - <strong>bons ou mauvais</strong> - sur les séries que vous connaissez. A partir de là, grâce à des statistiques, le site identifie quels sont les membres qui vous sont le plus proche. Vous l'aurez compris, ce sera ensuite sur l'avis de ces membres que se basera le site pour vous suggérer des séries.</p>
						<p>Pas besoin de <a href='/Controleur/Faq/Faq.php#la_notation'>noter</a> toutes les séries que vous connaissez d'une traite. Mais plus votre compte sera rempli, plus les suggestions seront précises.</p>
						<p>Comme vous le découvrirez bientôt, chaque série vous est suggéré avec un pourcentage. Ce pourcentage représente la probabilité que vous aimiez la série en question. Plus vous notez de série, plus ce nombre sera précis!</p>
						<p>Il y a un intérêt à court terme et un à long terme :<br />
						A court terme FYNS va vous permettre de faire le tri entre les séries à voir absolument pour vous, celles qu'il faut tenter et celles à éviter. Le site va également mettre en avant des séries moins médiatisées que vous avez pu manquer jusqu'ici.<br />
						Sur le plus long terme, vous n'aurez plus à fouiller pour trouver de nouvelles série. Sur FYNS, n'importe qui peut à chaque instant rajouter n'importe quelle série et la noter. Il suffit donc qu'une personne note une nouvelle série, pour qu'elle soit analysée par le système et vous soit proposée, si elle match votre profil. Donc à partir de maintenant, impossible de rater le lancement de ce qui aurait pu être votre nouvelle série référence, parce ce que si vous l'avez manqué, la communauté de FYNS sera là pour y penser pour vous!</p>
						
					</div>	
				</section>
				
			<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php"); ?>
		</div>
			
            <footer>
			
                <div id="Footer_site">
                    <h1>FYNS</h1>
                    <h2>Find Your Next Show</h2>
                    <p>Crée par Eric de La Varende</p>
                </div>
                <div id="Liens">
                    <h1>Liens utiles</h1>
						<ul>
							<li><a href="#">Accueil</a></li>
							<li><a href="#">Plan du site</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Séries</a></li>
						</ul>
                </div>
                <div id="Nouveaux_membres">
                    <h1>Nouveaux membres</h1>
                    <ul>
                        <li>Identifiant 1</li>
                        <li>Identifiant 2</li>
                        <li>Identifiant 3</li>
                        <li>Identifiant 4</li>
                    </ul>
                </div>
			
            </footer>
        
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/min.js"></script>
		<script src="/Vue/Index/Index.js"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		
    </body>
</html>