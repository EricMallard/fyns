	<div id='asideg'>

	<aside id="pop">
			<div>
					<h1>Séries Populaires</h1>	
						<ol>
							<?php
						
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Populaires.php");
							
							while ($donnees = $requ->fetch()){
								
								echo "<li id='pop".$donnees['id']."'>" .  $donnees['nom']  . "</li>";
							
							}
							?>
							
						
						
				
						</ol>
			</div>
	</aside>

	<aside id="premium">
			<div>
					<h1>Séries Premium</h1>	
						<ol>
							<?php
						
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Premium.php");
							
							while ($donnees = $requ->fetch()){
								
								echo "<li id='pre".$donnees['id']."'>" .  $donnees['nom']  . "</li>";
							
							}
							?>
							
						
						
				
						</ol>
			</div>
	</aside>

	<aside id="new">
			<div>
					<h1>Nouvelles Séries</h1>	
						<ol>
							<?php
						
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/New.php");
							
							while ($donnees = $requ->fetch()){
								
								echo "<li id='new".$donnees['id']."'>" .  $donnees['nom']  . "</li>";
							
							}
							?>
							
						
						
				
						</ol>
			</div>
	</aside>

	<aside id="hausse">
			<div>
				<h1>Séries en hausse</h1>
				<ol>
					<li>Première</li>
					<li>Deuxième</li>
					<li>Troisième</li>
					<li>Quatrième</li>
					<li>Cinquième</li>
				</ol>
			</div>
	</aside>

</div>