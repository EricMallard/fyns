

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="http://localhost/Fyns/min.css" />
		 <link rel="stylesheet" href="http://localhost/Fyns/Vue/Inscription/Inscription.css" />
		
        <title>Inscription</title>
	</head>


    <body class='IsHidden'>
        <div id="bloc_page">
		
            <?php include("../../Vue/Index/entete.php"); ?>
			<?php include($_SERVER['DOCUMENT_ROOT']."Fyns/Vue/Index/image.php"); ?>
            
			<div id='bas'>
			
				<p>Inscription Réussie</p>
				
				<aside id="pop">
						<div>
							<h1>Séries populaires</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="premium">
						<div>
							<h1>Séries Premium</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="new">
						<div>
							<h1>Nouvelles séries</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
				
				<aside id="hausse">
						<div>
							<h1>Séries en hausse</h1>
							<ol>
								<li>Première</li>
								<li>Deuxième</li>
								<li>Troisième</li>
								<li>Quatrième</li>
								<li>Cinquième</li>
							</ol>
						</div>
				</aside>
			
			</div>
			
            <footer>
                <div id="Footer site">
                    <h1>FYNS</h1>
                    <h2>Find Your Next Show</h2>
                    <p>Crée par Eric de La Varende</p>
                </div>
                <div id="Liens">
                    <h1>Liens utiles</h1>
						<ul>
							<li><a href="#">Accueil</a></li>
							<li><a href="#">Plan du site</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Séries</a></li>
						</ul>
                </div>
                <div id="Nouveaux membres">
                    <h1>Nouveaux membres</h1>
                    <ul>
                        <li>Identifiant 1</li>
                        <li>Identifiant 2</li>
                        <li>Identifiant 3</li>
                        <li>Identifiant 4</li>
                    </ul>
                </div>
            </footer>
        </div>
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="http://localhost/Fyns/nailthumb.js"></script>
		<script src="http://localhost/Fyns/min.js"></script>
		
    </body>
</html>