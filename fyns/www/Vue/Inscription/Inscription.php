

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		 <link rel="stylesheet" href="/Controleur/Inscription/Inscription.css" />
		 <link rel="icon" type="image/png" href="/FYNS.png" />
		
        <title>Inscription</title>
	</head>


    <body class='IsHidden'>
        <div id="bloc_page">
		
            <?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>
			<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
            
			<div id='bas'>
			
				<form method="post" action="#">
			
						<label for="pseudo">Pseudo :</label> <input type="text" name="pseudo" id="pseudo" required /><br />
						<label for="pass">Mot de passe :</label> <input type="password" name="pass" id="pass" required /><br />
						<label for="vpass">Vérif. MDP :</label> <input type="password" name="vpass" id="vpass" required /><br />
						<label for="email">E-mail :</label> <input type="email" name="email" id="email" required /><br />
						<div id='inscription_membre'>Inscription</div>
						<div id='rep'></div>
				
				</form>
				
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php"); ?>
			
			</div>
			
            <footer>
                <div id="Footer site">
                    <h1>FYNS</h1>
                    <h2>Find Your Next Show</h2>
                    <p>Crée par Eric de La Varende</p>
                </div>
                <div id="Liens">
                    <h1>Liens utiles</h1>
						<ul>
							<li><a href="#">Accueil</a></li>
							<li><a href="#">Plan du site</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Séries</a></li>
						</ul>
                </div>
                <div id="Nouveaux membres">
                    <h1>Nouveaux membres</h1>
                    <ul>
                        <li>Identifiant 1</li>
                        <li>Identifiant 2</li>
                        <li>Identifiant 3</li>
                        <li>Identifiant 4</li>
                    </ul>
                </div>
            </footer>
        </div>
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/min.js"></script>
		<script src="/Controleur/Inscription/Inscription.js"></script>
		
    </body>
</html>