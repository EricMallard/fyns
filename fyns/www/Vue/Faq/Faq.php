
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		<link rel="stylesheet" href="/Controleur/Faq/Faq.css" />
		<link rel="icon" type="image/png" href="/FYNS.png" />
        <title>Faq</title>
    </head>

    <body class='IsHidden'>
		
            <?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>
            
            <?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
            
			<div id="bas">


			
				<section id="faq">
						
					
					<div id='la_notation'>
						<h1>IMPORTANT : Comment noter?</h1>
					</div>
			
					<div>
						<h1>Autre</h1>
					</div>	
				</section>
				
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php");?>
				
				
			</div>
			
            <footer>
                <div id="Footer_site">
                    <h1>FYNS</h1>
                    <h2>Find Your Next Show</h2>
                    <p>Crée par Eric de La Varende</p>
                </div>
                <div id="Liens">
                    <h1>Liens utiles</h1>
						<ul>
							<li><a href="#">Accueil</a></li>
							<li><a href="#">Plan du site</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Séries</a></li>
						</ul>
                </div>
                <div id="Nouveaux_membres">
                    <h1>Nouveaux membres</h1>
                    <ul>
                        <li>Identifiant 1</li>
                        <li>Identifiant 2</li>
                        <li>Identifiant 3</li>
                        <li>Identifiant 4</li>
                    </ul>
                </div>
            </footer>
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/min.js"></script>
		
		
    </body>
</html>