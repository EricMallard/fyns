
<?php	

$req = $bdd->prepare('
SELECT id, nom, nbmembres, series.note, notem
FROM 
	(SELECT series_id, note AS notem FROM membres_series where membres_id = :membres_id) AS membres_note
	RIGHT JOIN series
	ON id = series_id
WHERE nom REGEXP "^[0-9]" 
ORDER BY nom');	 
$req->execute(array("membres_id" => $_SESSION['id']));

?>

