
<?php	

$requ = $bdd->prepare('
SELECT suggestions.fyns, series.nom, suggestions.serie_id AS id
FROM suggestions
	INNER JOIN series
	 ON serie_id = series.id
WHERE membre_id = :membres_id
ORDER BY suggestions.fyns DESC LIMIT 0, 5');
$requ->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
$requ->execute();	 

?>