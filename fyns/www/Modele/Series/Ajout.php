
<?php	

$req = $bdd->prepare('
INSERT INTO membres_series (membres_id, series_id, note, date_ajout)
VALUES (:membres_id, :series_id, :note, CURDATE())
');	 
$req->execute(array(
    ':membres_id' => $_SESSION['id'],
    'series_id' => $id_serie,
    'note' => $note));
	
	
$reque = $bdd->prepare('
SELECT AVG(note) as noteavg
FROM membres_series 
WHERE series_id = :series_id
');	 
$reque->execute(array(
    'series_id' => $id_serie
	));
$donnee = $reque->fetch();
	
$requ = $bdd->prepare('
UPDATE series 
SET note = :note, nbmembres = nbmembres + 1
WHERE (id = :id)
');	 
$requ->execute(array(
	'note' => $donnee['noteavg'],
    'id' => $id_serie));

?>