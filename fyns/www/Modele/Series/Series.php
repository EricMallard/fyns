
<?php	

if (isset($_SESSION['id']) AND !empty($_SESSION['id'])){
	$req = $bdd->prepare('
	SELECT id, nom, nbmembres, series.note, notem
	FROM 
		(SELECT series_id, note AS notem FROM membres_series where membres_id = :membres_id) AS membres_note
		RIGHT JOIN series
		ON id = series_id
		group by id
	ORDER BY nom
	');	 
	$req->execute(array("membres_id" => $_SESSION['id']));
}
else{
	$req = $bdd->query('
	SELECT id, nom, nbmembres, note
	FROM series
	group by id
	ORDER BY nom
	');
}

?>

