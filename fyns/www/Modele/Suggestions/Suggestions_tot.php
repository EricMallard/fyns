<?php
	session_start();

	
	$id = $_GET['id'];
	$id = trim($id);
	
	Include_once($_SERVER['DOCUMENT_ROOT'].'/Modele/ConnexionBase.php');

	//Nombre de membre
	$nbmembre = $bdd->query('SELECT COUNT(*) AS count FROM membres');
	$nb = $nbmembre->fetch();
	$nbmembre->closeCursor ();
	
	//Nombre de membre selectionnés pour les suggestions
	if ($nb['count'] < 102){
		$limit = $nb['count']-2;
	}
	elseif ($nb['count']/10 < 102){
		$limit = 100;
	}
	else {
		$limit = round($nb['count']/10);
	}

	$delete_sugg = $bdd->prepare('DELETE FROM suggestions WHERE membre_id = :id_membre');
	$delete_sugg->bindValue('id_membre', $id, PDO::PARAM_INT);
	$delete_sugg->execute();

	
	$notation_membres = $bdd->prepare('
		CREATE TEMPORARY TABLE IF NOT EXISTS classement_membres AS (
			select colonne1, sum(abs) AS note_membre


			from (select colonne1, colonne2, colonne3, membres_series.series_id as colonne4, membres_series.note as colonne5, (ifnull(2 -abs(colonne3 - membres_series.note),0))/count_series as abs

			from   (SELECT  colonne1, count_series, membres_series.series_id as colonne2, membres_series.note as colonne3
			     from (select membre1, membre2 as colonne1, count(series_id) as count_series
			           from membre_membres 
			           inner join membres_series
			           	on membre2 = membres_id
			           where membre1 = :session_id
			           group by membre2) as count_serie
			           
			     inner join membres_series 
			        on membre1 = membres_id) as colonnes123
			left join membres_series
				on membres_id = colonne1 and series_id = colonne2) as alle
			    
			    group by colonne1
		)'
	);
	$notation_membres->execute(array(
		'session_id' => $id));

	$max_min = $bdd->prepare('
		select min(note_membre), max(note_membre)
		into @min, @max
		from classement_membres'
	);
	$max_min->execute(array());

	$average = $bdd->prepare('
		select avg(colo1)
		into @avg
		from (SELECT (note_membre - @min)/(@max - @min) as colo1
		 FROM classement_membres 
		 WHERE note_membre <> 0 
		 ORDER BY note_membre DESC LIMIT :nbproche) as average'
	);
	$average->bindValue('nbproche', $limit, PDO::PARAM_INT);
	$average->execute();

	$avg = $bdd-> prepare('Select @avg');
	$avg_note = $avg->fetch();
	$avg->closeCursor ();
	echo $avg_note['@avg'];


	$sugg_ver1 = $bdd->prepare('
		CREATE TEMPORARY TABLE IF NOT EXISTS premiere_sugg AS (
			select colonne3, sum(nombre_exact) as nombre_exact_serie, sum(moy_pond) as moy_pond_serie, sum(colonne2) as nombre_serie
			from (select colonne1, colonne2, colonne3, colonne4, colonne2*colonne4 as moy_pond, membres_series.series_id IS NULL = 1 as nombre_exact 
			      from (Select colonne1, colonne2, membres_series.series_id as colonne3, membres_series.note as colonne4 
			            from (SELECT colonne1, (note_membre - @min)/(@max - @min) as colonne2
			                  FROM classement_membres 
			                  WHERE note_membre <> 0 
			                  ORDER BY note_membre DESC LIMIT :nbproche) as premiere 
			            inner JOIN membres_series 
			            ON colonne1 = membres_id) as serie_membre_proche 
			      left join membres_series 
			      on colonne3 = series_id and membres_id = :session_id) as infos 
			 where nombre_exact = 1 
			 group by colonne3)'
	);
	$sugg_ver1->bindValue('nbproche', $limit, PDO::PARAM_INT);
	$sugg_ver1->bindValue('session_id', $id, PDO::PARAM_INT);
	$sugg_ver1->execute();


	$sugg_ver2 = $bdd->prepare('
		insert into suggestions (membre_id, serie_id, nombre, note)
		select :session_id, colonne3, 50 + (nombre_serie/nombre_exact_serie)*25 + (nombre_serie/@avg)*(25/2) as new_nombre_serie, (moy_pond_serie/nombre_serie) as sugg_first

		from premiere_sugg'
	);
	$sugg_ver2->execute(array(
		'session_id' => $id));


	$sugg_ver3 = $bdd->prepare('
		UPDATE suggestions SET fyns = (note * nombre)/5 WHERE membre_id = :membre_id'
	);
	$sugg_ver3->execute(array(
		'membre_id' => $id));
	
	
	
	
	echo "Mise à jour terminée";
	
	?>
	
	
	
	
	
	
	
	

	
	