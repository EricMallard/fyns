<?php
	session_start();

	$_SESSION['id'] = $_GET['id'];
	echo $_SESSION['id'];
	
	Include_once($_SERVER['DOCUMENT_ROOT'].'/Modele/ConnexionBase.php');
	
		
		//Selection de tous les membres sauf le membre actif
		$totmembres = $bdd->prepare('SELECT id FROM membres WHERE id <> :membres_id ORDER BY id');
		$totmembres->execute(array(
			'membres_id' => $_SESSION['id']));
		
		$delete_membres = $bdd->prepare('DELETE FROM membre_membres WHERE membre1 = :id_membre');
		$delete_membres->bindValue('id_membre', $_SESSION['id'], PDO::PARAM_INT);
		$delete_membres->execute();
		
		$req = $bdd->prepare('UPDATE membre_membres set note_membre = :note_membre WHERE membre1 = :membre1 AND membre2 = :membre2');
		echo 'la';
		//Boucle sur l'ID de tous les membres sélec
		while ($donnees = $totmembres->fetch()){
			$note_membre = 0;
		
			//Selection des notes des séries du membres connecté
			$series = $bdd->prepare('SELECT series_id, note FROM membres_series WHERE membres_id = :membres_id ORDER BY series_id');
			$series->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
			$series->execute();
			echo 'ici';

			$donnees2 = $series->fetch();
			echo $donnees2['series_id'];
			
			//Boucle dans le membre proche en cours de toutes les séries du membre connecté
			while ($donnees2 = $series->fetch()){
			echo 'encore';
			echo $donnees2['series_id'];
				
				//La série en cours est elle dans le compte du membre proche?
				$series_m = $bdd->prepare('SELECT series_id, note FROM membres_series WHERE membres_id = :membres_id AND series_id = :series_id');
				$series_m->execute(array(
				'membres_id' => $donnees['id'], 
				'series_id' => $donnees2['series_id']));
				
				$result_serie = $series_m->fetch();
				
				//Si non perte de 0.5 point
				if (!$result_serie){
					echo "lol";

					$note_membre = $note_membre - 0.5;
				}
				//Si oui gain de point si la note est proche (diff <2) perte si la note est séparée de plus de 1.5 points
				else {
					$note_membre = $note_membre + (1.5-abs($donnees2['note']-$result_serie['note']));
				}
				
			}
			$series->closeCursor ();
			echo $note_membre;
			
			//Insertion de la note du membre proche en cours dans la table membres_membres
			$req->execute(array(
			'membre1' => $_SESSION['id'],
			'membre2' => $donnees['id'],
			'note_membre' => $note_membre
			));
			
			
		}
		$totmembres->closeCursor ();
		
		
		//Selection de la note maximum et minimum entre tous les membres, donc le membre le plus proche ainsi que le membre le plus loin
	$max = $bdd->prepare('SELECT max(note_membre) AS maxnote, min(note_membre) AS minnote FROM membre_membres WHERE membre1 = :membre1');
	$max->bindValue('membre1', $_SESSION['id'], PDO::PARAM_INT);
	$max->execute();
	
	$max_note = $max->fetch();
	$max->closeCursor ();
	
	$up_maxnote = $max_note['maxnote'] - $max_note['minnote'];
	
	//Note minimal à 0
	$work_note = $bdd->prepare('UPDATE membre_membres SET Note_fyns = note_membre - :min WHERE membre1 = :membre1');
	$work_note->bindValue('min', $max_note['minnote'], PDO::PARAM_INT);
	$work_note->bindValue('membre1', $_SESSION['id'], PDO::PARAM_INT);
	$work_note->execute();
	
	//Note maximal à 1, toutes les notes sont entre 0 et 1
	$work_note = $bdd->prepare('UPDATE membre_membres SET Note_fyns = Note_fyns / :max WHERE membre1 = :membre1');
	$work_note->bindValue('max', $up_maxnote, PDO::PARAM_INT);
	$work_note->bindValue('membre1', $_SESSION['id'], PDO::PARAM_INT);
	$work_note->execute();
	
	//Nombre de membre
	$nbmembre = $bdd->query('SELECT COUNT(*) AS count FROM membres');
	$nb = $nbmembre->fetch();
	$nbmembre->closeCursor ();
	
	//Nombre de membre selectionnés pour les suggestions
	if ($nb['count'] < 101){
		$nbproche = $nb['count']-1;
	}
	elseif ($nb['count']/10 < 101){
		$nbproche = 100;
	}
	else {
		$nbproche = round($nb['count']/10);
	}
	
	//Selection de la note moyenne des proches
	$avg = $bdd->prepare('SELECT avg(Note_fyns) AS average FROM membre_membres WHERE membre1 = :membre1');
	$avg->bindValue('membre1', $_SESSION['id'], PDO::PARAM_INT);
	$avg->execute();
	$avg_note = $avg->fetch();
	$avg->closeCursor ();
	
	
	
	//Selection des membres les plus proches et de leur note de proche
	$note_membre_sugg = $bdd->prepare('SELECT membre2, Note_fyns FROM membre_membres WHERE membre1 = :membres_id AND Note_fyns <> 0 ORDER BY Note_fyns DESC LIMIT 0, :nbproche');
	$note_membre_sugg->bindValue('nbproche', $nbproche, PDO::PARAM_INT);
	$note_membre_sugg->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
	$note_membre_sugg->execute();
	
	//Suppression des suggestions pour le membre connecté pour pouvoir réécrire	
	$delete_sugg = $bdd->prepare('DELETE FROM suggestions WHERE membre_id = :id_membre');
	$delete_sugg->bindValue('id_membre', $_SESSION['id'], PDO::PARAM_INT);
	$delete_sugg->execute();
	
	//Boucle sur les membres proches selectionnés
	while ($donnees = $note_membre_sugg->fetch()){
		
		//Sélection des séries et notes pour le membres proche en cours ainsi que le nom des séries 
		$series_membre_proche = $bdd->prepare('
		SELECT series_id, membres_series.note AS membre_serie_note, nom 
		FROM membres_series 
		INNER JOIN series
			ON series_id = series.id
		WHERE membres_id = :membres_id');
		$series_membre_proche->bindValue('membres_id', $donnees['membre2'], PDO::PARAM_INT);
		$series_membre_proche->execute();
		
		//Boucle sur les séries du membbre proche en cours
		while ($donnees2 = $series_membre_proche->fetch()){
			
			$nombre = 'nombre';
			$note_sugg = 'note_sugg';
			
			$series_connues = $bdd->prepare('SELECT membres_id FROM membres_series WHERE membres_id = :membre_id AND series_id = :series_id');
			$series_connues->bindValue('membre_id', $_SESSION['id'], PDO::PARAM_INT);
			$series_connues->bindValue('series_id', $donnees2['series_id'], PDO::PARAM_INT);
			$series_connues->execute();
			
			$result_serie = $series_connues->fetch();
			
			if (!$result_serie){
				//Si la série n'a pas encore de note on la fixe à 0
				if ((!isset (${$donnees2['nom'] . $nombre})) AND (!isset (${$donnees2['nom'] . $note_sugg}))){
					${$donnees2['nom'] . $nombre} = 0; //Popularité moyenne en % en fonction de la moyenne des notes des membres récupérés. Si les membres qui proposent cette série ne sont pas proche la popularité baisse et si ils sont peu à la suggérer, elle baisse aussi. Une bonne popularité s'obtient quand beaucoup de membres bien notés proposent cette série
					${$donnees2['nom'] . $note_sugg} = 0; //La note moyenne de la série pondéré par la note du membre
					${$donnees2['nom'] . $nombre_exact} = 0; //Le nombre de membres proches ayant suggéré la série
					
					$req = $bdd->prepare('INSERT INTO suggestions(membre_id, serie_id) VALUES(:membres_id, :series_id)');
					$req->execute(array(
					'membres_id' => $_SESSION['id'],
					'series_id' => $donnees2['series_id']
					));
				}
			
				${$donnees2['nom'] . $nombre} = ${$donnees2['nom'] . $nombre} + $donnees['Note_fyns'];
				${$donnees2['nom'] . $note_sugg} = ${$donnees2['nom'] . $note_sugg} + $donnees['Note_fyns']*$donnees2['membre_serie_note'];
				${$donnees2['nom'] . $nombre_exact} = ${$donnees2['nom'] . $nombre_exact} + 1;
			}
		}
		$series_membre_proche->closeCursor ();
	}
	$note_membre_sugg->closeCursor ();
	
	$series_sugg = $bdd->prepare('
	SELECT serie_id, series.nom AS nom
	FROM suggestions 
	INNER JOIN series
		ON serie_id = series.id
	WHERE membre_id = :membre_id');
	$series_sugg->bindValue('membre_id', $_SESSION['id'], PDO::PARAM_INT);
	$series_sugg->execute();
	
	$req = $bdd->prepare('UPDATE suggestions SET nombre = :nombre, note = :note, fyns = :fyns WHERE membre_id = :membres_id AND serie_id = :series_id');
	
	while ($donnees = $series_sugg->fetch()){
	
		
		$a = 0; //Note FYNS
		$b = 0; //Moyenne des membres ayant notés la série multiplié par 0.75 pour empecher un poids trop important. i.e. si seul le membre le plus proche a noté le poids sera de 75%
		${$donnees['nom'] . $note_sugg} = ${$donnees['nom'] . $note_sugg} / ${$donnees['nom'] . $nombre};
		$b = (${$donnees['nom'] . $nombre} / ${$donnees2['nom'] . $nombre_exact}) * 0.75;
		${$donnees['nom'] . $nombre} = (${$donnees['nom'] . $nombre} / $avg_note['average'])*(100/$nbproche);
		${$donnees['nom'] . $nombre} = $b * 100 + (1 - $b) * ${$donnees['nom'] . $nombre};
		$a = (${$donnees['nom'] . $note_sugg}*${$donnees['nom'] . $nombre})/5;
		
		
		
		
		$req->execute(array(
		'membres_id' => $_SESSION['id'],
		'series_id' => $donnees['serie_id'],
		'nombre' => ${$donnees['nom'] . $nombre},
		'note' => ${$donnees['nom'] . $note_sugg},
		'fyns' => $a
		));
	}
	
	
	$req = $bdd->prepare('
		SELECT suggestions.fyns, series.nom
		FROM suggestions
			INNER JOIN series
			 ON serie_id = series.id
		WHERE membre_id = :membres_id
		ORDER BY suggestions.fyns DESC LIMIT 0, 5');
	$req->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
	$req->execute();
	
	
	
	
	echo "Mise à jour terminée";
	
	?>
	
	
	
	
	
	
	
	

	
	