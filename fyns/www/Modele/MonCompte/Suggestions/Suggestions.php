
<?php	

$req = $bdd->prepare('
SELECT suggestions.note, suggestions.nombre, suggestions.fyns, series.nom, series.id AS id
FROM suggestions
	INNER JOIN series
	 ON serie_id = series.id
WHERE membre_id = :membres_id
ORDER BY suggestions.fyns DESC');
$req->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
$req->execute();	 

?>