
<?php	

$req = $bdd->prepare('
SELECT membres_series.note, series.nom, series.id AS id
FROM membres_series
	INNER JOIN series
	 ON series_id = series.id
WHERE membres_id = :membres_id
group by series_id
ORDER BY membres_series.note DESC');
$req->bindValue('membres_id', $_SESSION['id'], PDO::PARAM_INT);
$req->execute();	 

?>