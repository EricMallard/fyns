<?php /* get.php */
    $url = $_GET["Url"];
	$query = $_GET["query"];
	$language = $_GET["language"];

	$query = str_replace(" ","%20",$query);
	$url = htmlspecialchars($url);
	$query = htmlspecialchars($query);
	$language = htmlspecialchars($language);
	
	$url = $url.$query."&language=".$language;
    
	echo file_get_contents($url);
?>