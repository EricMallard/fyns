<?php
session_start();

?>

<!DOCTYPE html>
<html>

	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
		<meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		<link rel="stylesheet" href="/Controleur/Series/Series.css" />
		<link rel="stylesheet" href="/jquery.mCustomScrollbar.css" />
		<link rel="icon" type="image/png" href="/FYNS.png" />
        <title>Séries</title>
	</head>
	
	<body class='IsHidden'>
	
	
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>

		<div id="message_infos"> </div>
		
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
		
			<section id='barre_recherche'>
			
			
			
			
				<form id="recherchef"  method="post" >
				<input type="text" name="recherche" id="recherche" value="Recherche / Ajout" autocomplete="off"  />
				</form>
				
				<form id="Newshow" class="IsHidden"  method="post" >
				<input  id="addcancel" type="submit" value="Annuler" /><input  id="submitshow" type="submit" value="Créer une nouvelle série" /> 
				</form>
				
				
				<div id="flag">
					<div id="en" >
						<img  src="/Vue/images_diverses/flag_en.png" />
					</div>
					<div id="fr" class='IsHidden'>
						<img  src="/Vue/images_diverses/flag_fr.png" >
					</div>
				</div>
				<div id="resultats" class="IsHidden">
					
				</div>
				<br/>
				<div id="message" class="IsHidden"></div>
				
				
				
				
				<?php
				
				
				
				include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
				
				
				include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Series.php");
		
				
				echo "<br /><br />";
				
				
				?>
				
			</section>
			
			<div id="bas">
				<section id="Notation_series">

					<div id="middle">

						<div class="IsHidden" id="reponse_ajout">
							<table id="table2">
									<thead>
										<tr>
										   <th class='first'>Nom</th>
										   <th>Lancement</th>
										   <th>Chaine</th>
										   <th class='last'>+</th>
										   
										   
										</tr>
									</thead>
									
									<tbody id="bodytable">
									</tbody>
									
							</table>
						</div>
				
						<div id="fiche_serie" class="IsHidden">
							<div id='pour_pos'>
								<div id="img_serie" class="nailthumb-container"></div>
								<div id="info_serie" >
									<div id="overview_serie"></div>
									<div id="autre_serie">
										<div id="note_serie"><div class='bouton_info'>Note</div><div class='autre_info_serie_2'></div></div>
										<div id="date_serie"><div class='bouton_info'>Année</div><div class='autre_info_serie_2'></div></div>
										<div id="chaine_serie"><div class='bouton_info'>Chaine</div><div class='autre_info_serie_2'></div></div>
										<div id="genre_serie"><div class='bouton_info'>Genre</div><div class='autre_info_serie_2'></div></div>
										<div id="format_serie"><div class='bouton_info'>Format</div><div class='autre_info_serie_2'></div></div>
										<div id="statut_serie"><div class='bouton_info'>Statut</div><div class='autre_info_serie_2'></div></div>
										<div id="saison_serie"><div class='bouton_info'>Saison</div><div class='autre_info_serie_2'></div></div>
										<div id="bouton_note">Noter</div>
										
									</div>
								</div>
							</div>
						</div>

						<div id="fiche_serie_2" class="IsHidden">
								<div id="img_serie_2" class="nailthumb-container"></div>
								<div id="info_serie_2" >
									<div id="overview_serie_2" ></div>
									<div id="autre_serie_2">
										<div id="date_serie_2"><div class='bouton_info'>Année</div><div class='autre_info_serie_2'></div></div>
										<div id="chaine_serie_2"><div class='bouton_info'>Chaîne</div><div class='autre_info_serie_2'></div></div>
										<div id="genre_serie_2"><div class='bouton_info'>Genre</div><div class='autre_info_serie_2'></div></div>
										<div id="format_serie_2"><div class='bouton_info'>Format</div><div class='autre_info_serie_2'></div></div>
										<div id="statut_serie_2"><div class='bouton_info'>Statut</div><div class='autre_info_serie_2'></div></div>
										<div id="saison_serie_2"><div class='bouton_info'>Saison</div><div class='autre_info_serie_2'></div></div>
									</div>
								</div>
							</div>
						
						<div id="Series_site" >
						
							<table id="table">
								<thead>
								<tr>
								   <th class='first'>Nom</th>
								   <th>Suivie par</th>
								   <th>Note</th>
								   <th>Votre note</th>
								   <th class='last'>Gérer la note</th>
								</tr>
								</thead>
								
								<tbody id="tbody">
								
							<script src="Series2.js"></script>
								
							<?php
							
							
							$a = 0;
						
							while ($donnees = $req->fetch()){
								$a = $a +1;
								$j = $donnees['id'];
								echo "<tr id=".$j."><td>" . $donnees['nom'] . "</td><td>" .  $donnees['nbmembres'] . "</td><td>" .  $donnees['note']; ?> </td><td id="MaNote<?php echo $a?>" > <?php ; if (!isset($donnees['notem']) OR !$donnees['notem']){
																																										?> <form class="note" id="Note<?php echo $a;?>" method="post" action=" <?php echo "/Controleur/Series/Ajout.php?ajout=$j" ?>">
																																												<input id="note<?php echo $a;?>" class="Note" type="submit" value="Noter" />
																																												</form></td><td>
																																												
																																												<form class="IsHidden" id="Newnote<?php echo $a;?>" method="post" action=" <?php echo "/Controleur/Series/Ajout.php?ajout=$j" ?>">
																																												<input id="changetemp<?php echo $a;?>" class="Note" type="submit" value="Changer la note" />
																																												</form>
																																												</td></tr>
																																												
																																									<script type="text/javascript">
																																									
																																									var idserie = '<?php echo $j; ?>',
																																										numboucle = '<?php echo $a;?>';
			 
																																									Envoi(idserie,numboucle);
																																								
																																									 
																																									</script>
																																									
																																									<?php		
																																									}
																																									else {
																																										echo $donnees['notem']; ?> </td><td> <form class="note" id="Change<?php echo $a;?>" method="post" value="yo" action=" <?php echo "/Controleur/Series/Ajout.php?ajout=$j" ?>">
																																																				<input id="change<?php echo $a;?>" class="Note" type="submit" value="Changer la note" />
																																																				</form> </td></tr>
																																									
																																									
																																									<script type="text/javascript">
																																									
																																									var idserie = '<?php echo $j; ?>',
																																										numboucle = '<?php echo $a;?>';
																																										
																																									Change(idserie,numboucle);
																																									 
																																									</script>
																																									
																																									<?php
																																									}
																																									
																																									
								
							
							}
							$req->closeCursor ();
							?>
							</tbody>
							</table>
							<?php
							
						
								?>
						</div>
						
					</div>	

					<div id='right'>
						
						<?php 
						
						if (!empty($_SESSION['id'])){
							
							?>
							
							
							<div id="Classement">
								
								<?php
							
								include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
								include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Classement.php");
								
								?>
								<table id='truc'>
									<thead><tr>
									   <th class='first'>#</th>
									   <th>Nom</th>
									   <th >Votre note</th>
									   <th class='last'>Gestion</th>
									</tr>
									</thead>
									
									<tbody>
									
								<?php
								$a= 0;
								while ($donnees = $req->fetch()){
									
									$a = $a +1;
								
									echo "<tr id=\"Class" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . $donnees['note'];?> </td><td><input id="avoir" class="Note" type="submit" value="Changer" /></tr>
								<?php
								}
								?>
								</tbody>
								</table>
							
							</div>
							
							<div  id="notation" class='IsHidden'>
									
								<div id='name_show'></div>
								<div id='arriere'><img src="/Vue/images_diverses/back.png"/></div>
								<div id='avant'><img src="/Vue/images_diverses/next.png"/></div>
								
								
								<div id="unite" class="notejava"> 
									<div id="10" class="bouton_note a">0</div> 
									<div id="11" class="bouton_note a">1</div> 
									<div id="12" class="bouton_note a">2</div> 
									<div id="13" class="bouton_note a">3</div>
									<div id="14" class="bouton_note a">4</div>
									<div id="15" class="bouton_note a">5</div>
								</div>
								
								<div id="dix" class=" notejava">
									<div id="20" class="bouton_note b">0</div> 
									<div id="21" class="bouton_note b">1</div> 
									<div id="22" class="bouton_note b">2</div> 
									<div id="23" class="bouton_note b">3</div>
									<div id="24" class="bouton_note b">4</div>
									<div id="25" class="bouton_note b">5</div>
									<div id="26" class="bouton_note b">6</div>
									<div id="27" class="bouton_note b">7</div>
									<div id="28" class="bouton_note b">8</div>
									<div id="29" class="bouton_note b">9</div>
								</div>
								
								<div id="cent" class=" notejava">
									<div id="30" class="bouton_note b">0</div> 
									<div id="31" class="bouton_note b">1</div> 
									<div id="32" class="bouton_note b">2</div> 
									<div id="33" class="bouton_note b">3</div>
									<div id="34" class="bouton_note b">4</div>
									<div id="35" class="bouton_note b">5</div>
									<div id="36" class="bouton_note b">6</div>
									<div id="37" class="bouton_note b">7</div>
									<div id="38" class="bouton_note b">8</div>
									<div id="39" class="bouton_note b">9</div>
								
								</div>
								<form  id="noteserie" method="post" action=" <?php echo "/Controleur/Series/Ajout.php?ajout=$j" ?>">
								<input type="submit" id="notefinal" value="0.00" />
								</form>
									
								
							</div>
							
							<div id="Sugg">
								
								<?php
							
								include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Suggestions.php");
								
								?>
								<div id="suggdyn">
									<table>
										<thead>
										<tr>
										   <th class='first'>#</th>
										   <th>Nom</th>
										   <th>Note Fyns</th>
										   <th class='last'>Ajouter</th>
										</tr>
										
										</thead>
									
									<tbody>
										
									<?php
									$a= 0;
									while ($donnees = $requ->fetch()){
										
										$a = $a +1;
									
										echo "<tr id=\"Sugg" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . round($donnees['fyns'],0);?> %</td><td><input id="avoir" class="Note" type="submit" value="Noter" /></tr>
								<?php
									
									}
									?>
									</tbody>
									</table>
								</div>
							
							</div>
							
							
						<?php
						}
						?>
				
				</div>	
				</section>
	
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php");?>
				
			</div>	
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/footer.php");
				?>
			
		
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="/min.js"></script>
		<script src="Series.js"></script>
		<script src="Series3.js"></script>
		
	
	</body>
	
</html>