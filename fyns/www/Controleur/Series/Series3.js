var nb_message = 0;
var serie;
var boucle;
var ajoutorchange;
		
(function() {
    
	var searchElement = document.getElementById('recherche'),
	results = document.getElementById('resultats'),
	selectedResult = -1, // Permet de savoir quel résultat est sélectionné : -1 signifie "aucune sélection"
	previousRequest, // On stocke notre précédente requête dans cette variable
	previousValue = searchElement.value; // On fait de même avec la précédente valeur

	
	
	
	
	function getResults(keywords) { // Effectue une requête et récupère les résultats
	console.log(langue);
	$('#message_infos').css('top', '-40px');
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/Controleur/Series/Recherche.php?rech='+ encodeURIComponent(keywords));
	
	xhr.addEventListener('load', function() {
	
	displayResults(xhr.responseText);
	console.log(xhr.responseText);
	resultats.removeClass('IsHidden');
	$('#fiche_serie').addClass('IsHidden');
	
	
	}, false);
	
	xhr.send(null);
	
	return xhr;
	
	}
	
	function displayResults(response) { // Affiche les résultats d'une requête
	
		if (response >0) {
			results.innerHTML = '';
			
			if(nb_message <4){
				$('#message_infos').finish();
				$('#message_infos').text('Aucun résultat. Cliquez sur "Ajouter une série inexistante" si vous voulez l\'ajouter au site');
				var width_message = parseInt($('#message_infos').css('width'),10);
				var width_message2 = -width_message/2 + 'px';
				$('#message_infos').css('max-width', width_message + 20 + 'px').css('margin-left', width_message2);
				$('#message_infos').animate({top : '0px'}, 1000,function(){
					$(this).delay(3000).animate({top : '-40px'}, 1000);
				});
				nb_message++;
			}
		}
		else{ // On ne modifie les résultats que si on en a obtenu
			nb_message = 0;
			response = response.split('|');
			var responseLen = response.length;
			responseLen = responseLen/2;
			
			results.innerHTML = ''; // On vide les résultats
			
			var length = 40;
			for (var i = 0, div ; i < responseLen ; i++) {
			
				var tot = i + responseLen;
				div = results.appendChild(document.createElement('div'));
				div.innerHTML = response[i];
				div.setAttribute("id",response[tot] + "res");
				var string = $('#'+response[tot] + "res").text();
				if (string.length > length){
					length = string.length;
				}
				if (i == 0){
					$('#'+response[tot] + "res").css('padding-top','6px');
				}
				
				
				div.addEventListener('click', function(a) {
				chooseResult(a.target);
				}, false);
			
			}
			
			$('#resultats div').css('padding-left','11px').css('cursor','pointer').css('height','18px');
			
		}
		
		var div_ajout = results.appendChild(document.createElement('div'));
		div_ajout.innerHTML = "+ Ajouter une série inexistante";
		div_ajout.setAttribute("id","serie_inexistante");
		$('#serie_inexistante').
			css('padding-left','2px').
			css('padding-top','4px').
			css('cursor','pointer').
			css('height','18px').
			css('border-top','solid 1px rgb(200,204,246)').
			css('border-radius','3px');
		$('#recherche').css('width', '200px');
		$('#resultats').css('width', '200px');
		if (length > 40){
			$('#recherche').css('width', '250px');
			$('#resultats').css('width', '250px');
		}
		if (length > 60){
			$('#recherche').css('width', '300px');
			$('#resultats').css('width', '300px');
		}
		
		$('#serie_inexistante').click( function() {
			$('#Newshow').removeClass('IsHidden');
			$('#resultats').addClass('IsHidden');
			
			
		});
			
	
		
	
	}	
	
	
	function chooseResult(result) { // Choisi un des résultats d'une requête et gère tout ce qui y est attaché
	
		
		searchElement.value = previousValue = result.innerHTML; // On change le contenu du champ de recherche et on enregistre en tant que précédente valeur
		
		result.className = ''; // On supprime l'effet de focus
		selectedResult = -1; // On remet la sélection à "zéro"
		searchElement.focus(); // Si le résultat a été choisi par le biais d'un clique alors le focus est perdu, donc on le réattribue
		var idclick = result.getAttribute('id');//On récupère l'ID de la série choisie
		idclick = idclick.replace('res','');
		var image = new Image();
		$(image).attr("src","/Info_show/"+idclick+"/"+idclick+"poster.jpg");
		$(image).nailthumb({width:130,height:130});
		$(image).attr('id','poster_show');
		$('#img_serie').html(image);//on récupère le poster de la série et on l'affiche en thumbnail
		$('#poster_show').load(function(){
								$('#fiche_serie').show('slow');
								$('#resultats').addClass('IsHidden');
								$('html, body').animate({
									scrollTop: $("#barre_recherche").offset().top
								}, 1000);
								});
								var count_click_fiche_serie = 0;
								$(window).click(function(e){
									var target_fiche_serie = $(e.target).attr('id');
									if (($.contains(document.getElementById('fiche_serie'), e.target)) || (target_fiche_serie === 'fiche_serie')) {
										
									}
									else{
										count_click_fiche_serie++;
									}
									if ((count_click_fiche_serie == 2) && (!$.contains(document.getElementById('fiche_serie'), e.target)) && (target_fiche_serie !== 'fiche_serie')) {
										$('#fiche_serie').hide('slow');
									}

									
																					
								});
		$.post('/Controleur/Series/Fiche_min.php',//on envoie à un page php l'ID pour récupérer les infos sur la série dans mysql
				{ id: idclick, langue: langue },
			function(data){
				console.log(data);
				var Overview = $(data).find('Overview').text();
				$('#overview_serie').text(Overview);

				var Annee = $(data).find('annee').text();
				var Chaine = $(data).find('Chaine').text();
				var Genre = $(data).find('Genre').text();
				Genre = Genre.split('|');
				console.log(Genre);
				var Statut = $(data).find('Statut').text();
				var Format = $(data).find('Format').text();
				var Saison = $(data).find('saison').text();
				var Note = $(data).find('note').text();
				if ($(data).find('compte').text() > 0){
					$('#bouton_note').text('Changer la note');
					serie = idclick;
					console.log(serie);
					boucle = $('#'+serie).find('td').eq(3).attr('id');
					boucle = boucle.replace('MaNote', '');
					ajoutorchange = 2;
					$('#name_show').html($(data).find('nom'));
				}
				else{
					
					$('#bouton_note').text('Noter');
					serie = idclick;
					console.log(serie);
					boucle = $('#'+serie).find('td').eq(3).attr('id');
					boucle = boucle.replace('MaNote', '');
					ajoutorchange = 1;
					$('#name_show').html($(data).find('nom'));
				}

				$('#date_serie div:eq(1)').text(Annee);
				$('#chaine_serie div:eq(1)').text(Chaine);
				$('#genre_serie div:eq(1)').html("");
				var genreLen = Genre.length;
				if (genreLen >5){
					$('<div>'+Genre[1]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div >'+Genre[2]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div style="display: none; font-size:1.5em;">'+Genre[3]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div style="display: none;font-size:1.5em;">'+Genre[4]+'</div>').appendTo($('#genre_serie div:eq(1)'));
				}
				else if (genreLen == 5){
					$('<div>'+Genre[1]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div >'+Genre[2]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div style="display: none;font-size:1.5em;">'+Genre[3]+'</div>').appendTo($('#genre_serie div:eq(1)'));
				}
				else if (genreLen == 4){
					$('<div >'+Genre[1]+'</div>').appendTo($('#genre_serie div:eq(1)'));
					$('<div >'+Genre[2]+'</div>').appendTo($('#genre_serie div:eq(1)'));
				}
				else{
					$('<div >'+Genre[1]+'</div>').appendTo($('#genre_serie div:eq(1)'));
				}
				
				$('#statut_serie div:eq(1)').text(Statut);
				$('#format_serie div:eq(1)').text(Format);
				$('#saison_serie div:eq(1)').text(Saison);
				$('#note_serie div:eq(1)').text(Note);
				$('#recherche').css('width', '150px');
				$('#resultats').css('width', '150px');
			}
			
		);
		
	
	
	}
	
	
	
	searchElement.addEventListener('keyup', function(e) {
	
	var divs = results.getElementsByTagName('div');
	
	if (e.keyCode == 38 && selectedResult > -1) { // Si la touche pressée est la flèche "haut"
	
	divs[selectedResult--].className = '';
	
	if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
	divs[selectedResult].className = 'result_focus';
	}
	
	}
	
	else if (e.keyCode == 40 && selectedResult < divs.length - 1) { // Si la touche pressée est la flèche "bas"
	
	results.className = ''; // On affiche les résultats
	
	if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
	divs[selectedResult].className = '';
	}
	
	divs[++selectedResult].className = 'result_focus';
	
	}
	
	else if (e.keyCode == 13 && selectedResult > -1) { // Si la touche pressée est "Entrée"
	
	e.preventDefault();
	chooseResult(divs[selectedResult]);
	
	}
	
	else if (searchElement.value != previousValue) { // Si le contenu du champ de recherche a changé
	
	previousValue = searchElement.value;
	
	if (previousRequest && previousRequest.readyState < 4) {
	previousRequest.abort(); // Si on a toujours une requête en cours, on l'arrête
	}
	
	previousRequest = getResults(previousValue); // On stocke la nouvelle requête
	
	selectedResult = -1; // On remet la sélection à "zéro" à chaque caractère écrit
	
	}
	
	}, false);
	
	})();