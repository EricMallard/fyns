<?php
session_start();

?>

<!DOCTYPE html>
<html>

	<head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		<link rel="stylesheet" href="MesSeries.css" />
		<link rel="icon" type="image/png" href="/FYNS.png" />
        <title>Mes Séries</title>
    </head>
	
	<body class='IsHidden'>
	
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>
		
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>

		<div id="message_infos"> </div>
		
		<a id="myshows" href="/Controleur/MonCompte/MesSeries/MesSeries.php">Mes Séries</a>
		<a id="mysugg" href="/Controleur/MonCompte/Suggestions/Suggestions.php">Suggestions</a>
		
		<div id='bas'>
		
			<section id="classement_series">
			
				<div id='middle'>

					<div id="fiche_serie" class="IsHidden">
						<div id="hide" >x</div>
						<div id="img_serie" class="nailthumb-container"></div>
						<div id="info_serie" >
							<div id="overview_serie"></div>
							<div id="autre_serie">
								<div id="date_serie"></div>
								<div id="chaine_serie"></div>
								<div id="genre_serie"></div>
								<div id="format_serie"></div>
								<div id="statut_serie"></div>
								<div id="saison_serie"></div>
								<div id="note_serie"></div>
								<div id="nb_serie"></div>
								
							</div>
						</div>
					</div>
				
					<?php
					include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
					include_once($_SERVER['DOCUMENT_ROOT']."/Modele/MonCompte/MesSeries/MesSeries.php");
					
					?>
					<table id="table">
						<thead>
						<tr>
						    <th class='first'>Cl.</th>
							<th>Nom</th>
						    <th>Votre note</th>
							<th class='last'>Gestion</th>
						</tr>
						</thead>
						
						<tbody>
						
					<?php
					$a = 1;
					while ($donnees = $req->fetch()){
					
						echo "<tr id=\"Class" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . $donnees['note']; ?> </td><td><input  class="Note" type="submit" value="Changer la note" /> <img  class='suppression' src="/Vue/images_diverses/suppression.png" ></tr>
					<?php
					$a = $a +1;
					}
					?>
					</tbody>
					</table>

				</div>	
				<div id='right'>

					<div id="Sugg">
							
							<?php
						
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Suggestions.php");
							
							?>
							<div id="suggdyn">
								<table>
									<thead>
									<tr>
									   <th class='first'>#</th>
									   <th>Nom</th>
									   <th>Note Fyns</th>
									   <th class='last'>Ajouter</th>
									</tr>
									
									</thead>
								
								<tbody>
									
								<?php
								$a= 0;
								while ($donnees = $requ->fetch()){
									
									$a = $a +1;
								
									echo "<tr id=\"Sugg" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . round($donnees['fyns'],0);?> %</td><td><input id="avoir" class="Note" type="submit" value="Noter" /></tr>
							<?php
								
								}
								?>
								</tbody>
								</table>
							</div>
						
						</div>
					
					<div  id="notation" class='IsHidden'>
								
							<div id='name_show'></div>
							<div id='arriere'><img src="/Vue/images_diverses/back.png"/></div>
							<div id='avant'><img src="/Vue/images_diverses/next.png"/></div>
							
							
							<div id="unite" class="notejava"> 
								<div id="10" class="bouton_note a">0</div> 
								<div id="11" class="bouton_note a">1</div> 
								<div id="12" class="bouton_note a">2</div> 
								<div id="13" class="bouton_note a">3</div>
								<div id="14" class="bouton_note a">4</div>
								<div id="15" class="bouton_note a">5</div>
							</div>
							
							<div id="dix" class=" notejava">
								<div id="20" class="bouton_note b">0</div> 
								<div id="21" class="bouton_note b">1</div> 
								<div id="22" class="bouton_note b">2</div> 
								<div id="23" class="bouton_note b">3</div>
								<div id="24" class="bouton_note b">4</div>
								<div id="25" class="bouton_note b">5</div>
								<div id="26" class="bouton_note b">6</div>
								<div id="27" class="bouton_note b">7</div>
								<div id="28" class="bouton_note b">8</div>
								<div id="29" class="bouton_note b">9</div>
							</div>
							
							<div id="cent" class=" notejava">
								<div id="30" class="bouton_note b">0</div> 
								<div id="31" class="bouton_note b">1</div> 
								<div id="32" class="bouton_note b">2</div> 
								<div id="33" class="bouton_note b">3</div>
								<div id="34" class="bouton_note b">4</div>
								<div id="35" class="bouton_note b">5</div>
								<div id="36" class="bouton_note b">6</div>
								<div id="37" class="bouton_note b">7</div>
								<div id="38" class="bouton_note b">8</div>
								<div id="39" class="bouton_note b">9</div>
							
							</div>
							<form  id="noteserie" method="post" action="#">
							<input type="submit" id="notefinal" value="0.00" />
							</form>
								
							
						</div>
				</div>

			</section>
			
			<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php");?>
				
		</div>	
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/footer.php");
				?>
				
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/min.js"></script>
		<script src="MesSeries.js"></script>
		
	</body>
	
</html>