var boucle, serie, ajoutorchange, fiche_show = 0;



$('#table').css({width : $('#Sugg').offset().left - $('#table').offset().left - 10 + 'px'});


$('#fiche_serie').
	css('position','absolute').
	css('left','5px').
	css('top','-245px').
	css({width:  "590px", background: 'rgb(180,190,220)', height : '215px' }).
	css('font-weight','bold').
	css('font-size','0.8em').
	css('margin-left','20px').
	css('margin-bottom','10px').
	css('margin-top','10px').
	css('color', 'rgb(115,120,160)').
	css('border-radius', '5px').
	css('box-shadow', '1px 1px 1px rgb(115,120,160), -1px -1px 1px rgb(115,120,160)');
	
$('#overview_serie').
	css('width', 440 + 'px').
	css('position', 'absolute').
	css('left','150px').
	css('top','10px').
	css('height', '110px');
	
$('#hide').
	css('position', 'relative').
	css('left','578px').
	css('top','-2px').
	css('color', 'red').
	css('cursor', 'pointer');
	
$('#hide').click(function(){
	$('#fiche_serie').hide('slow');
	fiche_show = 0;
	
});
	
$('#img_serie').
	css('position', 'absolute').
	css('left','10px').
	css('top','10px');

var count_bouton = 0;
$('.bouton_note').each(function(){
	
	if (count_bouton > 5){
		if (count_bouton > 15){
			$(this).css({top: 10 + 2*15 + 2*8 + 'px'});
			$(this).css({left: 20 + (count_bouton-16)*11 + (count_bouton-16)*6 + 'px'});
		}
		else{
			$(this).css({top: 10 + 1* 15 + 1*8 + 'px'});
			$(this).css({left: 20 + (count_bouton-6)*11 + (count_bouton-6)*6 + 'px'});
		}
	}
	else{
		$(this).css({top: '10px'});
		$(this).css({left: 20 + count_bouton*11 + count_bouton*6 + 'px'});
	}
	
	count_bouton++;
});

$('#arriere').click(function(){
	if ($('#notefinal').attr('value') == 5){
		$('#unite div').removeClass('b').addClass('a');
	}
	if ($('#20').hasClass('a')){
		$('.a').removeClass('a').addClass('b');
		$('#unite div').removeClass('b').addClass('a');
	}
	if ($('#30').hasClass('a')){
		$('.a').removeClass('a').addClass('b');
		$('#dix div').removeClass('b').addClass('a');
	}
	if ($('#10').hasClass('b') && $('#20').hasClass('b') && $('#30').hasClass('b')){
		$('#cent div').removeClass('b').addClass('a');
	}
});

$('#avant').click(function(){
	if ($('#20').hasClass('a')){
		$('.a').removeClass('a').addClass('b');
		$('#cent div').removeClass('b').addClass('a');
	}
	if ($('#10').hasClass('a')){
		$('.a').removeClass('a').addClass('b');
		$('#dix div').removeClass('b').addClass('a');
	}
});


$('#table tbody').find('tr').each(function(i){
	
	$(this).find('td').each(function(index){
		if(i == 0){
			$(this).css('padding-top','8px');
		}
		if (index == 0){
			$(this).css('font-weight','bold').css('font-size','0.8em').css('padding-left','7px').css('width', '35px');
		}	
		else if (index == 1){
			$(this).css('text-align', 'left').css('width', '250px').css('font-weight','bold').css('font-size','0.8em');
		}
		else if (index == 2){
			$(this).css('text-align', 'center' ).css('width', '150px');
		}
		else{
			$(this).css('text-align', 'center' ).css('width', '200px');
			$(this).find('input').
				css('background', 'none').
				css('color', 'rgb(115,120,240)').
				css('border', 'solid 1px rgb(115,120,240)').
				css('border-radius', '5px').
				css('box-shadow', '1px 1px 1px rgb(115,120,240)').
				css("font-weight","bold");
			$(this).find('input').click(function(){
				var idserie = $('#table tbody').find('tr').eq(i).attr('id');
				idserie = idserie.replace('Class','');
				boucle = i;
				console.log(boucle);
				serie = idserie;
				ajoutorchange = 2;
				$("#unite div").removeClass('b').addClass('a');
				$("#dix div").removeClass('a').addClass('b');
				$("#cent div").removeClass('a').addClass('b');
				$.post('/Controleur/Series/Fiche_min.php',//on envoie à un page php l'ID pour récupérer les infos sur la série dans mysql
						{ id: idserie },
					function(data){
						var yoyo = $(data).find('nom').text();
						$('#name_show').html(yoyo);
				
					}
					
				);
				$('#notation').removeClass('IsHidden');
			});
			$(this).find('img').click(function(){
				var idserie = $('#table tbody').find('tr').eq(i).attr('id');
				idserie = idserie.replace('Class','');
				boucle = i;
				serie = idserie;
				
				$.post('/Controleur/MonCompte/MesSeries/Suppression.php',//on envoie à un page php l'ID pour récupérer les infos sur la série dans mysql
						{ serie: idserie },
					function(data){
						console.log(data);
						if (data > 0) {
							
							$("#Class"+idserie+" input").remove();
							$("#Class"+idserie+" img").remove();
							$("#Class"+idserie).css({fontSize:'0em', border:'opx'});
							$("#Class"+idserie).find('td').each(function(){$(this).hide('slow')});
							$("#Class"+idserie).hide('slow');

							
							$('#message_infos').text('La série a été supprimée de votre compte');
							var width_message = parseInt($('#message_infos').css('width'),10);
							var width_message2 = -width_message/2 + 'px';
							$('#message_infos').css('max-width', width_message + 20 + 'px').css('margin-left', width_message2);
							$('#message_infos').animate({top : '0px'}, 1000,function(){
								$(this).delay(2000).animate({top : '-40px'}, 1000);
							});
							
							$.post('/Modele/Suggestions/Suggestions.php', 
								function(data){
									console.log(data);
									var length = $(data).find('serie').length;
									console.log(length);
									$('#suggdyn tbody').find('tr').each(function(index2){
										console.log(index2);
										console.log(length);
										if (index2 + 1 > length){
											console.log('ahah');
											$(this).find('td').eq(0).text("");
											$(this).find('td').eq(1).text("");
											$(this).find('td').eq(2).text("");
											$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
											$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
											$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
										}
										else{
											console.log('aha');
											$(this).find('td').eq(0).text($(data).find('num').eq(index2).text());
											$(this).find('td').eq(1).text($(data).find('nom').eq(index2).text());
											$(this).find('td').eq(2).text($(data).find('fyns').eq(index2).text());
											$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
											$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
											$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
										}
									});
								}
							);
						}
						else{


							$('#message_infos').text('Votre série n\'a pas été supprimée, veuillez réessayer');
							var width_message = parseInt($('#message_infos').css('width'),10);
							var width_message2 = -width_message/2 + 'px';
							$('#message_infos').css('max-width', width_message + 20 + 'px').css('margin-left', width_message2);
							$('#message_infos').animate({top : '0px'}, 1000,function(){
								$(this).delay(2000).animate({top : '-40px'}, 1000);
							});
						}
						
				
					}
					
				);
			});
		}
		
	});
});

$('#table thead').find('tr').each(function(){
	$(this).find('th').each(function(index){
		if (index == 0){
			$(this).css('padding-left','5px').css('width', '25px');
		}
		else if (index == 1){
			$(this).css('width', '200px').css('text-align', 'left');
			
		}
		else if (index == 2){
			$(this).css('width', '130px').css('text-align', 'center');
		}
		
		else{
			$(this).css('text-align', 'center').css('width', '150px');
		}
	});
});




$('#suggdyn table tbody').find('tr').each(function(i){
	$(this).find('td').each(function(index){
		if(i == 0){
			$(this).css('padding-top','8px');
		}
		if (index == 0){
			$(this).css('font-weight','bold').css('font-size','0.8em').css('padding-left','10px').css('padding-right','10px');
		}
		else if (index == 1){
			$(this).css('text-align', 'left').css('width', '100px');
			$(this).click(function(){
				var sugg_show = $('#suggdyn table tbody').find('tr').eq(i).attr('id');
				sugg_show = sugg_show.replace('Sugg','');
				var image = new Image();
				$(image).attr("src","/Info_show/"+sugg_show+"/"+sugg_show+"poster.jpg");
				$(image).nailthumb({width:130,height:130});
				$(image).attr('id','poster_show');
				$('#img_serie').html(image);//on récupère le poster de la série et on l'affiche en thumbnail
				$('#poster_show').load(function(){
										
										$('#fiche_serie').show('slow');
										fiche_show = 250;
										$(window).one('click',function(e){
								
											if ((!$.contains(document.getElementById('fiche_serie'), e.target))){
												
												$('#fiche_serie').hide('slow');
												fiche_show = 0;
																						
												
											}
								
									
					
										});
										
										
				});
				$.post('/Controleur/Series/Fiche_min.php',//on envoie à un page php l'ID pour récupérer les infos sur la série dans mysql
						{ id: sugg_show },
					function(data){
						var Overview = $(data).find('Overview').text();
						$('#overview_serie').html(Overview);
					}
					
				);
				
			});
		}
		else if (index == 2){
			$(this).css('text-align', 'center').css('width', '120px');
			
		}
		else {
			$(this).css('text-align', 'center' ).css('width', '80px');
			$(this).find('input').
				css('background', 'none').
				css('color', 'rgb(115,120,240)').
				css('border', 'solid 1px rgb(115,120,240)').
				css('border-radius', '5px').
				css('box-shadow', '1px 1px 1px rgb(115,120,240)').
				css("font-weight","bold");
			$(this).find('input').each(function(){
				$(this).click(function(){
					var serie_class = $('#suggdyn table tbody').find('tr').eq(i).attr('id');
					serie = serie_class.replace('Sugg','');
					ajoutorchange = 1;
					$('#notation').removeClass('IsHidden');
					$("#unite div").removeClass('b').addClass('a');
					$("#dix div").removeClass('a').addClass('b');
					$("#cent div").removeClass('a').addClass('b');
					$.post('/Controleur/Series/Fiche_min.php',//on envoie à un page php l'ID pour récupérer les infos sur la série dans mysql
							{ id: serie },
						function(data){
							var yoyo = $(data).find('nom').text();
							$('#name_show').html(yoyo);
					
						}
						
					);
				});
			});
		}
		
	});
});

$('#suggdyn table thead').find('tr').each(function(){
	$(this).find('th').each(function(index){
		if (index == 0){
			$(this).css('padding-left','5px').css('padding-right','15px');
		}
		else if (index == 1){
			$(this).css('width', '100px').css('text-align', 'left');
			
		}
		else if (index == 2){
			$(this).css('width', '120px').css('text-align', 'center');
			
		}
		else {
			$(this).css('width', '80px').css('text-align', 'center');
		}
		
	});
});





											
											
var	query;
	

							
$.fn.followTo = function (origin, fix) {
    var $this = this,
        $window = $(window);

		
		
		
	

    $window.scroll(function (e) {
    	var offset = $('#table').offset();
    	var offset2 =  $('footer').offset().top;
    	
		
		var a = 0;
		
        if ($window.scrollTop() < offset.top) {
            $this.css({
                position: 'relative',
                top: origin,
                left: '0px'
            });
		}
		else if ($window.scrollTop() > offset2 - 505) {
			
            $this.css({
				
                position: 'relative',
                top: offset2 -505 + origin -offset.top,
                left: '0px'
            });
        }
		else {
            $this.css({
                position: 'fixed',
                top: fix,
                left: $('#Sugg').offset().left
            });
        }
    });
};





$('#Sugg').followTo(0,0);
$('#notation').followTo(35,192);





var unite=0, dix=0, cent=0;

$("#unite div").click(function(){
							if ($('#10').hasClass('a')){
								unite = $('#unite div').index($(this));
								if (unite > 4) {
									dix = 0;
									cent = 0;
								};
								$('.a').removeClass('a').addClass('b');

								$("#notefinal").attr('value', unite + "." + dix + cent);
								if ($("#notefinal").attr('value') < 5) {
									$("#dix div").removeClass('b').addClass('a');
								};

							}
							
});

$("#dix div").click(function(){
							if ($('#20').hasClass('a')){
								dix = $('#dix div').index($(this));
								$('.a').removeClass('a').addClass('b');
								$("#cent div").removeClass('b').addClass('a');
								$("#notefinal").attr('value',unite + "." + dix + cent);
							}
							
});

$("#cent div").click(function(){
							if ($('#30').hasClass('a')){
								cent = $('#cent div').index($(this));
								$('.a').removeClass('a').addClass('b');
								$("#notefinal").attr('value',unite + "." + dix + cent);
							}
});

$("#notefinal").click(function(e){
							var notefinale,
								prevnote;
							e.preventDefault();
							console.log(ajoutorchange);
							notefinale = $(this).attr('value');
							prevnote = $('#table tbody tr:eq('+boucle+')').find("td").eq().html();
							$('#table tbody tr:eq('+boucle+')').find("td").eq(2).html(notefinale);
							if(ajoutorchange == 1){
								$.post('/Controleur/Series/Ajout.php',{ note: notefinale, ajout: serie },
									function(data){
										$.post('/Modele/Suggestions/Suggestions.php', { note: notefinale, series_id: serie },
											function(data){
												console.log(data);
												var length = $(data).find('serie').length;
												console.log(length);
												$('#suggdyn tbody').find('tr').each(function(index2){
													if (index2 + 1 > length){
														console.log('ahah');
														$(this).find('td').eq(0).text("");
														$(this).find('td').eq(1).text("");
														$(this).find('td').eq(2).text("");
														$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
														$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
														$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
													}
													else{
														console.log('aha');
														$(this).find('td').eq(0).text($(data).find('num').eq(index2).text());
														$(this).find('td').eq(1).text($(data).find('nom').eq(index2).text());
														$(this).find('td').eq(2).text($(data).find('fyns').eq(index2).text());
														$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
														$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
														$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
													}
												});
													
												
											}
										);
									}
								);
							}
							if(ajoutorchange == 2){
								$.post('/Controleur/Series/Change.php',{ nnote: notefinale, change: serie },
									function(data){
										$.post('/Modele/Suggestions/Suggestions.php', 
											function(data){
												console.log(data);
												var length = $(data).find('serie').length;
												console.log(length);
												$('#suggdyn tbody').find('tr').each(function(index2){
													console.log(index2);
													console.log(length);
													if (index2 + 1 > length){
														console.log('ahah');
														$(this).find('td').eq(0).text("");
														$(this).find('td').eq(1).text("");
														$(this).find('td').eq(2).text("");
														$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
														$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
														$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
													}
													else{
														console.log('aha');
														$(this).find('td').eq(0).text($(data).find('num').eq(index2).text());
														$(this).find('td').eq(1).text($(data).find('nom').eq(index2).text());
														$(this).find('td').eq(2).text($(data).find('fyns').eq(index2).text());
														$(this).find('td').eq(0).data("store",$(this).find('td').eq(0).text());
														$(this).find('td').eq(1).data("store",$(this).find('td').eq(1).text());
														$(this).find('td').eq(2).data("store",$(this).find('td').eq(2).text());
													}
												});
											}
										);
									}
								);
							}
							
							$("#notation").addClass("IsHidden");
							$("#notefinal").attr('value', 0+ "." +0+0);
							$("#unite div").removeClass('b').addClass('a');
							$("#dix div").removeClass('a').addClass('b');
							$("#cent div").removeClass('a').addClass('b');
							unite=0;
							dix=0; 
							cent=0;
							
							
							
						});
		






$('body').removeClass('IsHidden');

											
