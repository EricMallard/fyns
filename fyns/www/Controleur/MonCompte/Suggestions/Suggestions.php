<?php
session_start();

?>

<!DOCTYPE html>
<html>

	<head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/min.css" />
		<link rel="stylesheet" href="Suggestions.css" />
		<link rel="stylesheet" href="/jquery.mCustomScrollbar.css" />
		<link rel="icon" type="image/png" href="/FYNS.png" />
        <title>Suggestions</title>
    </head>
	
	<body class='IsHidden'>
	
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/entete.php"); ?>
		
		<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/image.php"); ?>
		
		<a id="myshows" href="/Controleur/MonCompte/MesSeries/MesSeries.php">Mes Séries</a>
		<a id="mysugg" href="/Controleur/MonCompte/Suggestions/Suggestions.php">Suggestions</a>
	
		<div id="bas">
			
			
			<section id="classement_sugg">
			
				<div id='middle'>

					<div id="fiche_serie_2" class="IsHidden">
						<div id="img_serie_2" class="nailthumb-container"></div>
						<div id="info_serie_2" >
							<div id="overview_serie_2" ></div>
							<div id="autre_serie_2">
								<div id="date_serie_2"></div>
								<div id="chaine_serie_2"></div>
								<div id="genre_serie_2"></div>
								<div id="format_serie_2"></div>
								<div id="statut_serie_2"></div>
								<div id="saison_serie_2"></div>
								<div id="note_serie_2"></div>
								<div id="nb_serie_2"></div>
								
							</div>
						</div>
					</div>

				
					<?php
					include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
					include_once($_SERVER['DOCUMENT_ROOT']."/Modele/MonCompte/Suggestions/Suggestions.php");
					?>
					<table id='table'>
					<thead>
					<tr>
						   <th class='first'>Cl.</th>
						   <th>Nom</th>
						   <th>Volume</th>
						   <th>Qualité</th>
						   <th>Fyns</th>
						   <th class="last">Notation</th>
						</tr>
					</thead>
					
					<tbody>
						
					<?php
					$a = 0;
					while ($donnees = $req->fetch()){
						$a = $a + 1;
						echo "<tr id=\"Sugg" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . round($donnees['nombre'],0) . "%</td><td>" . $donnees['note'] . "</td><td>" . round($donnees['fyns'],0); ?> %</td><td><input  class="Note" type="submit" value="Noter" /></tr> 
					<?php
					}
					?>
					</tbody>
					</table>
				</div>

				<div id='right'>
				
					<div id="Classement">
							
							<?php
						
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/ConnexionBase.php");
							include_once($_SERVER['DOCUMENT_ROOT']."/Modele/Series/Classement.php");
							
							?>
							<table id='truc'>
								<thead><tr>
								   <th class='first'>#</th>
								   <th>Nom</th>
								   <th >Votre note</th>
								   <th class='last'>Gestion</th>
								</tr>
								</thead>
								
								<tbody>
								
							<?php
							$a= 0;
							while ($donnees = $req->fetch()){
								
								$a = $a +1;
							
								echo "<tr id=\"Class" . $donnees['id'] . "\"><td>" . $a . "</td><td>" . $donnees['nom'] . "</td><td>" . $donnees['note'];?> </td><td><input id="avoir" class="Note" type="submit" value="Changer" /></tr>
							<?php
							}
							?>
							</tbody>
							</table>
						
					</div>
						
					<div  id="notation" class='IsHidden'>
							
						<div id='name_show'></div>
						<div id='arriere'><img src="/Vue/images_diverses/back.png"/></div>
						<div id='avant'><img src="/Vue/images_diverses/next.png"/></div>
						
						
						<div id="unite" class="notejava"> 
							<div id="10" class="bouton_note a">0</div> 
							<div id="11" class="bouton_note a">1</div> 
							<div id="12" class="bouton_note a">2</div> 
							<div id="13" class="bouton_note a">3</div>
							<div id="14" class="bouton_note a">4</div>
							<div id="15" class="bouton_note a">5</div>
						</div>
						
						<div id="dix" class=" notejava">
							<div id="20" class="bouton_note b">0</div> 
							<div id="21" class="bouton_note b">1</div> 
							<div id="22" class="bouton_note b">2</div> 
							<div id="23" class="bouton_note b">3</div>
							<div id="24" class="bouton_note b">4</div>
							<div id="25" class="bouton_note b">5</div>
							<div id="26" class="bouton_note b">6</div>
							<div id="27" class="bouton_note b">7</div>
							<div id="28" class="bouton_note b">8</div>
							<div id="29" class="bouton_note b">9</div>
						</div>
						
						<div id="cent" class=" notejava">
							<div id="30" class="bouton_note b">0</div> 
							<div id="31" class="bouton_note b">1</div> 
							<div id="32" class="bouton_note b">2</div> 
							<div id="33" class="bouton_note b">3</div>
							<div id="34" class="bouton_note b">4</div>
							<div id="35" class="bouton_note b">5</div>
							<div id="36" class="bouton_note b">6</div>
							<div id="37" class="bouton_note b">7</div>
							<div id="38" class="bouton_note b">8</div>
							<div id="39" class="bouton_note b">9</div>
						
						</div>
						<form  id="noteserie" method="post" action="#">
						<input type="submit" id="notefinal" value="0.00" />
						</form>
							
						
					</div>
				</div>
						
						
					
			</section>
			
			<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/asideg.php");?>
				
		</div>	
				<?php include($_SERVER['DOCUMENT_ROOT']."/Vue/Index/footer.php");
				?>
				
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/nailthumb.js"></script>
		<script src="/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="/min.js"></script>
		<script src="Suggestions.js"></script>
		
	</body>
	
</html>