$('#connexion_membre').hover(function(e){
								e.preventDefault();
								var pseudo = $('#pseudo').val(),
								pass = $('#pass').val(),
								auto = $('#auto').val();
								$.post('/Controleur/Connexion/Connexion.php',{ pseudo: pseudo, pass: pass, auto: auto, hover : 1 },
																function(data){
																	
																	if (data > 0){
																		$('#connexion_membre').css('color','green').css('border', 'solid 1px green');
																		
																		$.get('/Modele/Suggestions/Suggestions_tot.php', { id: data },
																										function(info){
																											console.log(info);
																										}
																		);
																		$('#connexion_membre').one('click', function(){
																			$.post('/Controleur/Connexion/Connexion.php',{ pseudo: pseudo, pass: pass, auto: auto },
																				function(){
																					window.location = "/Controleur/Series/Series.php";
																				}
																			);
																		});
																	}
																	else{
																		$('#rep').text('Mot de passe ou identifiant erroné');
																	}
																}
								);
							},function(){
								$('#connexion_membre').css('color','rgb(149,157,238)').css('border', 'solid 1px rgb(149,157,238)');
								$('#rep').text('');
								$('#connexion_membre').unbind('click');
							}
);