<?php
namespace fyns\UserBundle\AjoutUser;

use fyns\SuggestionsBundle\Entity\UserUser;

class fynsAjoutUserInscription
{

	protected $em;

	public function __construct(\Doctrine\ORM\EntityManager $em, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage)
	{
	  $this->em = $em;
	  $this->user = $tokenStorage->getToken()->getUser();
	}

	public function ajoutUser($listUsers)
    {
	    
  		$em = $this->em;
  		$user = $this->user;

  		
		foreach ($listUsers as $listUser)
		{
			$userUser = new UserUser;
			$userUser->setUser1($user);
			$userUser->setUser2($listUser);
			$em->persist($userUser);

			$userUser2 = new UserUser;
			$userUser2->setUser1($listUser);
			$userUser2->setUser2($user);
			$em->persist($userUser2);
		}
		$em->flush();

	}
}
