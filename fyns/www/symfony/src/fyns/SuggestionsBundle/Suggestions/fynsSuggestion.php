<?php
namespace fyns\SuggestionsBundle\Suggestions;

use Doctrine\ORM\EntityManager;

class fynsSuggestion
{

	protected $em;

	public function __construct(\Doctrine\ORM\EntityManager $em, \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage)
	{
	  $this->em = $em;
	  $this->user = $tokenStorage->getToken()->getUser();
	}

	public function Suggestions()
    {
	    
  		$em = $this->em;
  		$user = $this->user;
  		$user = $user->getId();

  		$repository = $em->getRepository('fynsSuggestionsBundle:UserSugg');
			

		$listSuggs = $repository->findBy(array('user' => $user));
		foreach ($listSuggs as $listSugg)
		{
			$em->remove($listSugg);
		}
		$em->flush();

		
		$connection = $em->getConnection();
		$statement = $connection->executeUpdate
		(
			
			"UPDATE user_user
			INNER JOIN
			( 
				SELECT user2, sum(abs) AS note_membre
				FROM 
				(
					SELECT user2, showId, noteShow, user_show.note AS noteother, (ifnull(2 -abs(noteShow - user_show.note),0))/count_series_user2 AS abs
					FROM  
					(
						SELECT  user2, count_series_user2, show_id AS showId, user_show.note AS noteShow
				     	FROM 
				     	(
				     		SELECT user1_id, user2_id AS user2, count(show_id) AS count_series_user2
				            FROM user_user
				            INNER JOIN user_show
				           	ON user2_id = user_id
				            WHERE user1_id = :user
				            GROUP BY user2_id
				        ) AS count_serie
				           
				        INNER JOIN user_show 
				        ON user1_id = user_id
				    ) AS colonnes123

					LEFT JOIN user_show
					ON user_id = user2 AND show_id = showId
				) AS alle
				GROUP BY user2

			) AS AA
			ON user_user.user2_id = AA.user2




			SET user_user.note = AA.note_membre			
			WHERE user_user.user1_id = :user    

			", 
			array
			(
				'user' => $user 
			)
		);

		$repository = $em->getRepository('fynsSuggestionsBundle:UserUser')
		;

		$maxMin = $repository->notemaxmin();
		$maxMin = $maxMin['0'] ;
		$max = $maxMin['max_note'];
		$min = $maxMin['min_note'];

		$statement3 = $connection->prepare("
					
					
						
		SELECT avg(colo1) as avg
		
		FROM
		(    SELECT (note - :min)/(:max - :min) as colo1
			 FROM user_user 
			 WHERE note <> 0 
			 ORDER BY note DESC
		 ) as average
				        

		");
		
		$statement3->bindValue('min', $min);
		$statement3->bindValue('max', $max);
		$statement3->execute();
		$average = $statement3->fetchAll();
		$average = $average['0'];
		$average = $average['avg'];

		$statement5 = $connection->executeUpdate
		(
			
			"INSERT INTO user_sugg (user_id, show_id, nombre, note)
			
			
			SELECT :session_id, user2_show, 50 + (nombre_serie/nombre_exact_serie)*25 + (nombre_serie/:avg)*(25/2) as new_nombre_serie, (moy_pond_serie/nombre_serie) AS sugg_first
			FROM
			(
				SELECT user2_show, sum(nombre_exact) AS nombre_exact_serie, sum(moy_pond) AS moy_pond_serie, sum(note_user) AS nombre_serie
			    FROM 
			    (
			    	SELECT user2, note_user, user2_show, user2_show_note, note_user*user2_show_note AS moy_pond, user_show.show_id IS NULL = 1 AS nombre_exact 
			        FROM
			        (
			        	SELECT user2, note_user, user_show.show_id AS user2_show, user_show.note AS user2_show_note 
			            FROM
			            (
			            	SELECT user2_id AS user2, (note - :min)/(:max - :min) AS note_user
			                FROM user_user 
			                WHERE user1_id = :session_id AND note <> 0 
			                ORDER BY note DESC 
			            
			            ) AS premiere 
			            INNER JOIN user_show 
			            ON user2 = user_id

			        ) AS serie_membre_proche 
			        LEFT JOIN user_show 
			        ON user2_show = show_id AND user_id = :session_id

			    ) AS infos 
			    WHERE nombre_exact = 1 
			    GROUP BY user2_show

			)AS prem_sugg

			   

			", 
			array
			(
				'min' => $min,
				'max' => $max,
				'avg' => $average,
				'session_id' => $user
			)
		);
	}
}
