<?php

namespace fyns\SuggestionsBundle\Controller;

require_once __DIR__.'/../../../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use ZipArchive;
use SimpleXMLElement;
use fyns\SuggestionsBundle\Entity\Shows;
use DateTime;
use fyns\SuggestionsBundle\Entity\UserShow;

class SeriesController extends Controller
{
    public function rechercheseriesAction(Request $request)
    {
    	$serie = $request->request->get('serie');
    	$serie = str_replace(" ","%20",$serie);
    	$language = $request->request->get('language');

    	$url = 'http://thetvdb.com/api/GetSeries.php?seriesname=' . $serie . "&language=" . $language;
		
	    
		return new Response(file_get_contents($url));
    }

    public function getzipAction(Request $request)
    {
    	$serieid = $request->request->get('serieid');
    	$language = $request->request->get('language');


    	$url = 'http://thetvdb.com/api/F588DBB2B96C9C4C/series/' . $serieid . '/all/' . $language . '.zip';

    	$fs = new Filesystem();

    	if (!$fs->exists($this->get('kernel')->getRootDir() . '/../upload/infoshow/' . $serieid)) 
    	{
			$fs->mkdir($this->get('kernel')->getRootDir() . '/../upload/infoshow/' . $serieid, 0700);
		};
		

		file_put_contents($this->get('kernel')->getRootDir() . '/../upload/infoshow/'.$serieid."/".$serieid.$language.".zip", file_get_contents($url));
	
		$zip = new ZipArchive;
		$res = $zip->open($this->get('kernel')->getRootDir() . '/../upload/infoshow/'.$serieid."/".$serieid.$language.".zip");
		if ($res === TRUE) {
			
			$zip->extractTo($this->get('kernel')->getRootDir() . '/../upload/infoshow/'.$serieid);
			$zip->close();
		} else {
			echo 'failed, code:' . $res;
		}
	    
		
		return new Response('ok');

    }

    public function newshowAction (Request $request)
    {

    	$serieid = $request->request->get('serieid');
    	$language = $request->request->get('language');
    	$overview = $request->request->get('overview');
    	$status = $request->request->get('status');
    	$begin = $request->request->get('begin');
    	$begin = DateTime::createFromFormat('Y-m-j', $begin);
    	$chaine = $request->request->get('chaine');
    	$format = $request->request->get('format');
    	$episode = $request->request->get('episode');
    	$saison = $request->request->get('saison');
    	$nom = $request->request->get('nom');
    	$genre = $request->request->get('genre');

    	$show = new Shows();

    	$show->setIdTvdb($serieid);
    	$show->setnom($nom);
    	$show->setannee($begin);
    	$show->setsaison($saison);
    	$show->setepisode($episode);
    	$show->setformat($format);
    	$show->setchaine($chaine);
    	$show->setlangue($language);
    	$show->setstatut($status);
    	$show->setgenre($genre);
    	$show->setoverview($overview);

    	$em = $this->getDoctrine()->getManager();
    	$em->persist($show);
    	$em->flush();

    	return new Response('ok');
    	
    }

    public function newseriemembreAction (Request $request)
    {

    	$em = $this->getDoctrine()->getManager();

    	$serieid = $request->request->get('serieid');
    	$note = $request->request->get('note');
    	
    	$serieid = $em->getRepository('fynsSuggestionsBundle:Shows')->find($serieid);

    	$user = $this->container->get('security.token_storage')->getToken()->getUser();

        $userShow = $em->getRepository('fynsSuggestionsBundle:UserShow')->checkNoteSerieUser($serieid, $user);

    	if($userShow)
        {
            $userShow->setNote($note);
        }
        else
        {
            $userShow = new UserShow();
        	$userShow->setUser($user);
        	$userShow->setShow($serieid);
        	$userShow->setNote($note);
            
        	$em->persist($userShow);
        }

    	$em->flush();

    	return new Response('ok');

    }
}




