<?php

namespace fyns\SuggestionsBundle\Controller;

use fyns\SuggestionsBundle\Entity\membres;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use fyns\SuggestionsBundle\Form\membresType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$repository = $this
		  ->getDoctrine()
		  ->getManager()
		  ->getRepository('fynsSuggestionsBundle:Shows')
		;

		$listShows = $repository->findAll();

		$sugg = $this->container->get('fyns_suggestions.suggestion');
		$sugg->Suggestions();

		$em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
		$connection = $em->getConnection();
		$statement2 = $connection->prepare("
			
			
				
				     		SELECT user1_id as user1, user2_id AS user2, count(show_id) AS count_series_user2
				            FROM user_user
				            INNER JOIN user_show
				           	ON user2_id = user_id
				            WHERE user1_id = :yo
				            GROUP BY user2_id
				        

		");
		$statement2->bindValue('yo', 2);
		$statement2->execute();
		$listmemb = $statement2->fetchAll();

		return $this->render('fynsSuggestionsBundle:Default:index.html.twig', array( 'listShows' => $listShows, 'listmemb' => $listmemb));
    }

    public function inscriptionAction(Request $request)
    {
    	// On crée un objet Advert
	    $membres = new membres();

	    $form   = $this->get('form.factory')->create(membresType::class, $membres);

	    // On crée le FormBuilder grâce au service form factory
	    

	    // Si la requête est en POST
	    if ($request->isMethod('POST')) {
	      // On fait le lien Requête <-> Formulaire
	      // À partir de maintenant, la variable $advert contient les valeurs entrées dans le formulaire par le visiteur
	      $form->handleRequest($request);

	      // On vérifie que les valeurs entrées sont correctes
	      // (Nous verrons la validation des objets en détail dans le prochain chapitre)
	      if ($form->isValid()) {
	        // On enregistre notre objet $advert dans la base de données, par exemple
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($membres);
	        $em->flush();

	        // On redirige vers la page de visualisation de l'annonce nouvellement créée
	        return $this->redirectToRoute('fyns_suggestions_index');
	      }
	    }

	    // On passe la méthode createView() du formulaire à la vue
	    // afin qu'elle puisse afficher le formulaire toute seule
	    return $this->render('fynsSuggestionsBundle:Default:inscription.html.twig', array(
	      'form' => $form->createView(),
	    ));
    }
}


