<?php

namespace fyns\SuggestionsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserShow
 *
 * @ORM\Table(name="user_show")
 * @ORM\Entity(repositoryClass="fyns\SuggestionsBundle\Repository\UserShowRepository")
 */
class UserShow
{
    

    /**
     * @var int
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAjout", type="datetime")
     */
    private $dateAjout;

    /**
       * @ORM\Id
       * @ORM\ManyToOne(targetEntity="fyns\UserBundle\Entity\User")
       * @ORM\JoinColumn(nullable=false)
       */
      private $user;

      /**
       * @ORM\Id
       * @ORM\ManyToOne(targetEntity="fyns\SuggestionsBundle\Entity\Shows")
       * @ORM\JoinColumn(nullable=false)
       */
      private $show;

    public function __construct()
    {
        $this->dateAjout = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set note
     *
     * @param integer $note
     *
     * @return UserShow
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     *
     * @return UserShow
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    

    /**
     * Set user
     *
     * @param \fyns\UserBundle\Entity\User $user
     *
     * @return UserShow
     */
    public function setUser(\fyns\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \fyns\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * Set show
     *
     * @param \fyns\SuggestionsBundle\Entity\Shows $show
     *
     * @return UserShow
     */
    public function setShow(\fyns\SuggestionsBundle\Entity\Shows $show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return \fyns\SuggestionsBundle\Entity\Shows
     */
    public function getShow()
    {
        return $this->show;
    }
}
