<?php

namespace fyns\SuggestionsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserShow
 *
 * @ORM\Table(name="user_sugg")
 * @ORM\Entity(repositoryClass="fyns\SuggestionsBundle\Repository\UserSuggRepository")
 */
class UserSugg
{
    

    /**
     * @var int
     *
     * @ORM\Column(name="nombre", type="decimal", nullable=true)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="note", type="decimal", nullable=true)
     */
    private $note;


    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="fyns\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="fyns\SuggestionsBundle\Entity\Shows")
     * @ORM\JoinColumn(nullable=false)
     */
    private $show;

        
}
