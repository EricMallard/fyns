<?php

namespace fyns\SuggestionsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shows
 *
 * @ORM\Table(name="shows")
 * @ORM\Entity(repositoryClass="fyns\SuggestionsBundle\Repository\ShowsRepository")
 */
class Shows
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tvdb", type="integer")
     */
    private $idTvdb;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="annee", type="date", nullable=true)
     */
    private $annee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ajout", type="date")
     */
    private $dateAjout;

    /**
     * @var int
     *
     * @ORM\Column(name="saison", type="integer", nullable=true)
     */
    private $saison;

    /**
     * @var int
     *
     * @ORM\Column(name="episode", type="integer", nullable=true)
     */
    private $episode;

    /**
     * @var int
     *
     * @ORM\Column(name="format", type="integer", nullable=true)
     */
    private $format;

    /**
     * @var string
     *
     * @ORM\Column(name="chaine", type="string", length=30, nullable=true)
     */
    private $chaine;

    /**
     * @var string
     *
     * @ORM\Column(name="langue", type="string", length=10)
     */
    private $langue;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=50, nullable=true)
     */
    private $genre;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=30, nullable=true)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="overview", type="string", length=20000, nullable=true)
     */
    private $overview;

    public function __construct()
    {
        $this->dateAjout = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTvdb
     *
     * @param integer $idTvdb
     *
     * @return Shows
     */
    public function setIdTvdb($idTvdb)
    {
        $this->idTvdb = $idTvdb;

        return $this;
    }

    /**
     * Get idTvdb
     *
     * @return int
     */
    public function getIdTvdb()
    {
        return $this->idTvdb;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Shows
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set annee
     *
     * @param \DateTime $annee
     *
     * @return Shows
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return \DateTime
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     *
     * @return Shows
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Set saison
     *
     * @param integer $saison
     *
     * @return Shows
     */
    public function setSaison($saison)
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * Get saison
     *
     * @return int
     */
    public function getSaison()
    {
        return $this->saison;
    }

    /**
     * Set episode
     *
     * @param integer $episode
     *
     * @return Shows
     */
    public function setEpisode($episode)
    {
        $this->episode = $episode;

        return $this;
    }

    /**
     * Get episode
     *
     * @return int
     */
    public function getEpisode()
    {
        return $this->episode;
    }

    /**
     * Set format
     *
     * @param integer $format
     *
     * @return Shows
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return int
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set chaine
     *
     * @param string $chaine
     *
     * @return Shows
     */
    public function setChaine($chaine)
    {
        $this->chaine = $chaine;

        return $this;
    }

    /**
     * Get chaine
     *
     * @return string
     */
    public function getChaine()
    {
        return $this->chaine;
    }

    /**
     * Set langue
     *
     * @param string $langue
     *
     * @return Shows
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return Shows
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Shows
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set overview
     *
     * @param string $overview
     *
     * @return Shows
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;

        return $this;
    }

    /**
     * Get overview
     *
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }
}

