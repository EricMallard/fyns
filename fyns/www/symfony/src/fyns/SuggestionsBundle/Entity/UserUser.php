<?php

namespace fyns\SuggestionsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserUser
 *
 * @ORM\Table(name="user_user")
 * @ORM\Entity(repositoryClass="fyns\SuggestionsBundle\Repository\UserUserRepository")
 */
class UserUser
{
    

    /**
     *
     * @ORM\Column(name="note", type="decimal", precision=3, scale=2, nullable=true)
     */
    private $note;


    /**
       * @ORM\Id
       * @ORM\ManyToOne(targetEntity="fyns\UserBundle\Entity\User")
       * @ORM\JoinColumn(nullable=false)
       */
      private $user1;

      /**
       * @ORM\Id
       * @ORM\ManyToOne(targetEntity="fyns\UserBundle\Entity\User")
       * @ORM\JoinColumn(nullable=false)
       */
      private $user2;

    

    

    /**
     * Set note
     *
     * @param integer $note
     *
     * @return UserUser
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return integer
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set user1
     *
     * @param \fyns\UserBundle\Entity\User $user1
     *
     * @return UserUser
     */
    public function setUser1(\fyns\UserBundle\Entity\User $user1)
    {
        $this->user1 = $user1;

        return $this;
    }

    /**
     * Get user1
     *
     * @return \fyns\UserBundle\Entity\User
     */
    public function getUser1()
    {
        return $this->user1;
    }

    /**
     * Set user2
     *
     * @param \fyns\UserBundle\Entity\User $user2
     *
     * @return UserUser
     */
    public function setUser2(\fyns\UserBundle\Entity\User $user2)
    {
        $this->user2 = $user2;

        return $this;
    }

    /**
     * Get user2
     *
     * @return \fyns\UserBundle\Entity\User
     */
    public function getUser2()
    {
        return $this->user2;
    }
}
