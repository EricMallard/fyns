var language = 'en';
var serie = $('#serie');
var tmdb = $('#recherche'),
	query;


tmdb.click(function(e)
{
	query = serie.val();
	$.post(//On lance une requete vers une page php qui va faire une requete inter domaine vers thetvdb, pour récupérer les id!
		url,
		{serie: query, language: language},
		function(data) 
		{
			if ($(data).find("Series").length)
			{
				$(data).find("Series").each(function(index)
				{//On propose les différentes séries trouvés
					if ($(this).find("Overview").length > 0)
					{
						var SeriesName = $(this).find("SeriesName").text();
						var SeriesId = $(this).find("seriesid").text();
						
						$('#result').append('<div class="getzip" id="'+SeriesId+'">'+SeriesName+'</div>')
					}
				});
			}
		}
	);

});

$('#result').on(
	'click',
	'.getzip',
	function(e)
	{
		query = $(this).attr('id');
		$.post
		(
			url2,
			{serieid: query, language: language},
			function() 
			{
				
				$.post
				(
					url3 + '../../upload/infoshow/' + query + '/en.xml',
					function(xml)
					{
						var Overview = $(xml).find('Overview').eq(0).text();
						var Name = $(xml).find('SeriesName').eq(0).text();
						var Status = $(xml).find('Status').eq(0).text();
						var Beginning = $(xml).find('FirstAired').eq(0).text();
						var Genre = $(xml).find('Genre').eq(0).text();
						var Chaine = $(xml).find('Network').eq(0).text();
						var Format = $(xml).find('Runtime').eq(0).text();
						var countepisodes = 0;
						var last_saison_overwrite = 0;
						$(xml).find('Episode').each(function(index){//Petite boucle pour trouver le nombre d'épisodes
																	var saison_overwrite = $(this).find('Combined_season').text();
																	if (saison_overwrite > 0){
																		countepisodes++;
																	}
																	if (last_saison_overwrite < saison_overwrite){
																		last_saison_overwrite = saison_overwrite
																	}
						});

						$.post
						(

							url4,
							{
								serieid: query, 
								language: language,
								overview: Overview,
								status: Status,
								genre: Genre,
								begin: Beginning,
								chaine: Chaine,
								format: Format,
								episode: countepisodes,
								saison: last_saison_overwrite,
								nom: Name

							}


						)
					}
				)
				
			}
		);

	}
);

$(".serie_click").one
(
	'click',
	function(){

		var id_serie = $(this).attr('id').replace('add', '');
		$(this).append('<input type="number" name="noteSerie" id="noteSerie' + id_serie + '" /><div id="submitNote' + id_serie + '" class="add_serie_membre">Noter</div>');

	}
);


$('#listshow').on
(
	'click',
	'.add_serie_membre',
	function()
	{	

		var id_serie = $(this).attr('id').replace('submitNote', '');
		var note = $('#noteSerie'+id_serie).val();
		console.log(note);
		$.post
		(

			url5,
			{
				serieid: id_serie,
				note: note
			}

		)

	}
);																

							




											
